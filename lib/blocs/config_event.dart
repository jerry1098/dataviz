import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class ConfigEvent extends Equatable {
  const ConfigEvent();
}

class ConfigButtonPressed extends ConfigEvent {
  final String port;

  const ConfigButtonPressed({
    @required this.port
});

  @override
  List<Object> get props => [port];

  @override
  String toString() {
    return 'ConfigButtonPressed{port: $port}';
  }


}
