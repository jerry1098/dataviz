import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class LayoutState extends Equatable {
  const LayoutState();
  @override
  List<Object> get props => [];
}

class InitialLayoutState extends LayoutState {}

class ErrorLayoutState extends LayoutState {

  final String error;

  ErrorLayoutState({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() {
    return 'ErrorLayoutState{error: $error}';
  }
}

class InitializedLayoutState extends LayoutState {
  final List<Widget> widgets;
  final bool gridVisible;

  InitializedLayoutState({@required this.widgets, @required this.gridVisible});

  @override
  List<Object> get props => [widgets, gridVisible];

  @override
  String toString() {
    return 'InitializedLayoutState{widgets: $widgets, gridVisible: $gridVisible}';
  }
}
