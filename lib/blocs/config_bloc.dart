import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:data_vis/backend/DataHandler.dart';
import 'package:meta/meta.dart';
import './bloc.dart';

class ConfigBloc extends Bloc<ConfigEvent, ConfigState> {
  final DataHandler dataHandler;

  ConfigBloc({@required this.dataHandler}) : assert(dataHandler != null);

  @override
  ConfigState get initialState => InitialConfigState();

  @override
  Stream<ConfigState> mapEventToState(
    ConfigEvent event,
  ) async* {
    if (event is ConfigButtonPressed) {
      yield InitializingConfigState();

      try {
        String port = event.port;
        int intPort = int.tryParse(port);
        if (intPort != null && intPort > 0 && intPort <= 65535) {
          await dataHandler.initializeDataHandler(intPort);
          yield InitializedConfigState();
        } else if (port.length == 0){
          yield NoPortConfigState();
          await dataHandler.initializeDataHandler(4444);
          yield InitializedConfigState();
        } else {
          yield WrongPortConfigState();
        }
      } catch (error) {
        yield ErrorConfigState(error: error.toString());
      }
    }
  }
}
