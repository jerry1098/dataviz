import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:data_vis/backend/DataHandler.dart';
import 'package:data_vis/backend/LayoutHandler.dart';
import 'package:flutter/material.dart';
import './bloc.dart';
import 'package:meta/meta.dart';

class LayoutBloc extends Bloc<LayoutEvent, LayoutState> {
  final DataHandler dataHandler;
  final LayoutHandler layoutHandler;

  LayoutBloc({@required this.dataHandler, @required this.layoutHandler})
      : assert(dataHandler != null && layoutHandler != null);

  @override
  LayoutState get initialState => InitialLayoutState();

  @override
  Stream<LayoutState> mapEventToState(
    LayoutEvent event,
  ) async* {
    if (event is NewCanvasSize) {
      layoutHandler.sizeX = event.x;
      layoutHandler.sizeY = event.y;
      layoutHandler.xOffset = event.xOffset;
      layoutHandler.yOffset = event.yOffset;
      if (layoutHandler.getWidgetListLength() > 0) {
        List<Widget> _widgets = await layoutHandler.getWidgets();
        //print(_widgets);
        yield InitializedLayoutState(widgets: _widgets, gridVisible: false);
      } else {
        yield InitialLayoutState();
      }
    }
    if (event is AddNewPanelLayoutEvent) {
      layoutHandler.addWidget(this);
      yield InitializedLayoutState(widgets: await layoutHandler.getWidgets(), gridVisible: true);
    }
    if (event is GestureLayoutEvent) {
      layoutHandler.gestureDetected(event.x, event.y, event.eventType);
      yield InitializedLayoutState(widgets: await layoutHandler.getWidgets(), gridVisible: event.eventType == 1);
    }
  }
}
