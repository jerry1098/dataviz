import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class ConfigState extends Equatable {
  const ConfigState();

  @override
  List<Object> get props => [];
}

class InitialConfigState extends ConfigState {}

class InitializingConfigState extends ConfigState {}

class ErrorConfigState extends ConfigState {
  final String error;

  const ErrorConfigState({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() {
    return 'ErrorConfigState{error: $error}';
  }
}

class InitializedConfigState extends ConfigState {}

class NoPortConfigState extends ConfigState {}

class WrongPortConfigState extends ConfigState {}