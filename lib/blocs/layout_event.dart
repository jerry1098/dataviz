import 'package:data_vis/widgets/panel.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class LayoutEvent extends Equatable {
  const LayoutEvent();

  @override
  List<Object> get props => [];
}

class AddNewPanelLayoutEvent extends LayoutEvent {}

class RemovePanelLayoutEvent extends LayoutEvent {

  final int panelID;

  RemovePanelLayoutEvent({@required this.panelID});

  @override
  List<Object> get props => [panelID];

  @override
  String toString() {
    return 'RemovePanelLayoutEvent{id: $panelID}';
  }
}

class GestureLayoutEvent extends LayoutEvent {

  final double x;
  final double y;
  final int eventType;

  GestureLayoutEvent({@required this.x, @required this.y, @required this.eventType});

  @override
  List<Object> get props => [x, y, eventType];

  @override
  String toString() {
    return 'MovePanelLayoutEvent{offsetX: $x, offsetY: $y, eventType: $eventType}';
  }
}

class ResizePanelLayoutEvent extends LayoutEvent {
  
  final Panel panel;
  final double offsetX;
  final double offsetY;

  ResizePanelLayoutEvent({@required this.panel, @required this.offsetX, @required this.offsetY});

  @override
  List<Object> get props => [panel, offsetX, offsetY];

  @override
  String toString() {
    return 'ResizePanelLayoutEvent{panel: $panel, offsetX: $offsetX, offsetY: $offsetY}';
  }
}

class NewCanvasSize extends LayoutEvent {

  final double x;
  final double y;
  final double xOffset;
  final double yOffset;

  NewCanvasSize({@required this.x, @required this.y, @required this.xOffset, @required this.yOffset});

  @override
  List<Object> get props => [x, y];

  @override
  String toString() {
    return 'NewCanvasSize{x: $x, y: $y}';
  }
}
