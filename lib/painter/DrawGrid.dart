import 'package:data_vis/blocs/bloc.dart';
import 'package:flutter/material.dart';

class DrawGrid extends CustomPainter {
  Paint _paint;
  LayoutBloc layoutBloc;
  final double opacity;

  DrawGrid({@required this.layoutBloc, @required this.opacity}) {
    _paint = Paint()
        ..color = Color.fromRGBO(181, 181, 181, opacity)
        ..strokeWidth = 2.0
        ..style = PaintingStyle.fill;
  }

  @override
  void paint(Canvas canvas, Size size) {
    double xOffset = (size.width % 25.0) / 2;
    double yOffset = (size.height % 25.0) / 2;
    double spaceAroundX = xOffset;
    double spaceAroundY = yOffset;
    layoutBloc.add(NewCanvasSize(x: size.width, y: size.height, xOffset: xOffset, yOffset: yOffset));
    if (opacity == 0.0) return;
    while (xOffset <= size.width) {
      canvas.drawLine(Offset(xOffset, spaceAroundY), Offset(xOffset, size.height - spaceAroundY), _paint);
      xOffset += 25.0;
    }
    while (yOffset <= size.height) {
      canvas.drawLine(Offset(spaceAroundX, yOffset), Offset(size.width - spaceAroundX, yOffset), _paint);
      yOffset += 25.0;
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}