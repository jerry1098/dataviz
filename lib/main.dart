import 'package:data_vis/backend/DataHandler.dart';
import 'package:data_vis/backend/LayoutHandler.dart';
import 'package:data_vis/pages/config_page.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Data Visualizer',
      theme: ThemeData(
        accentColor: Color.fromRGBO(0, 129, 138, 1),
        primaryColor: Color.fromRGBO(40, 49, 73, 1),
      ),
      home: MyHomePage(),
      debugShowCheckedModeBanner: false,
      showPerformanceOverlay: true,
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage({Key key}) : super(key: key);
  final dataHandler = DataHandler();
  @override
  Widget build(BuildContext context) {
    return ConfigPage(dataHandler: dataHandler);
  }
}