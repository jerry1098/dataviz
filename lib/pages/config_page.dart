import 'package:data_vis/backend/DataHandler.dart';
import 'package:data_vis/backend/LayoutHandler.dart';
import 'package:data_vis/blocs/bloc.dart';
import 'package:data_vis/widgets/config_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


class ConfigPage extends StatelessWidget {
  final DataHandler dataHandler;

  ConfigPage({Key key, @required this.dataHandler}) : assert (dataHandler != null  ), super(key: key);

  @override
  Widget build(BuildContext context) {
    MediaQueryData query = MediaQuery.of(context);
    return Scaffold(
      //appBar: AppBar(title: Text('Initial Config')),
      backgroundColor: Color.fromRGBO(40, 49, 73, 1),
      body: BlocProvider(
        create: (context) {
          return ConfigBloc(dataHandler: dataHandler);
        },
        child: Center(
          child: Container(
            height: query.size.height - query.size.height / 1.618,
            width: query.size.width - query.size.width / 1.618,
            child: ConfigCard(dataHandler: dataHandler),
          ),
        ),
      )
    );
  }
}
