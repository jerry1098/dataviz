import 'dart:async';

import 'package:data_vis/backend/DataHandler.dart';
import 'package:data_vis/backend/LayoutHandler.dart';
import 'package:data_vis/widgets/parent_grid.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:data_vis/blocs/bloc.dart';
import 'package:intl/intl.dart';

class MainPage extends StatelessWidget {

  final DataHandler dataHandler;
  final layoutHandler = LayoutHandler();

  MainPage({@required this.dataHandler});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) {
        return LayoutBloc(dataHandler: dataHandler, layoutHandler: layoutHandler);
      },
      child: MainPageScaffold(),
    );
  }
}

class MainPageScaffold extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MainPageScaffoldState();
  }
}

class MainPageScaffoldState extends State<MainPageScaffold> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: mainAppBar(context),
      backgroundColor: Color.fromRGBO(40, 49, 73, 1),
      body: ParentGrid(),
    );
  }
}

AppBar mainAppBar(BuildContext context) {
  return AppBar(
    title: Text('Data Visualizer'),
    centerTitle: true,
    elevation: 0.0,
    actions: <Widget>[
      _Time()
    ],
    leading: Tooltip(
      message: 'Create new panel',
      child: FlatButton(
        child: Icon(Icons.add, color: Colors.white),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(600.0)
        ),
        onPressed: () {
          BlocProvider.of<LayoutBloc>(context).add(AddNewPanelLayoutEvent());
        },
      ),
    ),
  );
}

class _Time extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _TimeState();
  }
}

class _TimeState extends State<_Time> {

  String _timeString;

  @override
  Widget build(BuildContext context) {
    return Center(child: Padding(
      padding: const EdgeInsets.only(right: 12.0),
      child: Text(_timeString, style: TextStyle(fontSize: 22.0, fontStyle: FontStyle.italic),),
    ));
  }

  void _getTime() {
    final String formattedDateTime = _formatDateTime(DateTime.now());
    setState(() {
      _timeString = formattedDateTime;
    });
  }

  String _formatDateTime(DateTime dateTime) {
    return DateFormat('HH:mm:ss').format(dateTime);
  }

  @override
  void initState() {
    _timeString = _formatDateTime(DateTime.now());
    Timer.periodic(Duration(milliseconds: 1000), (timer) {_getTime();});
    super.initState();
  }
}