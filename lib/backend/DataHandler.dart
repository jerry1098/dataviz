class DataHandler {
  Future<String> getTrashData() async {
    return Future.delayed(Duration(seconds: 5), () {return 'some trash';});
  }

  Future<void> initializeDataHandler(int port) {
    print('Initializing DataHandler with port: $port');
    return Future.delayed(Duration(milliseconds: 500));
  }
}