
import 'package:data_vis/blocs/bloc.dart';
import 'package:data_vis/panelWidgets/test_panel.dart';
import 'package:data_vis/widgets/panel.dart';
import 'package:flutter/material.dart';

class LayoutHandler {

  double _sizeX = 0.0;
  double _sizeY = 0.0;

  double _xOffset = 0.0;
  double _yOffset = 0.0;

  List<Panel> _widgets = List();

  Panel currentlyDragging;
  double lastX;
  double lastY;

  void gestureDetected(double x, double y, int eventType) {
    if (eventType == 0) {
      currentlyDragging = isWidgetAtPosition(x, y);
      print(currentlyDragging);
      if (currentlyDragging != null) {
        _widgets.removeAt(_widgets.indexOf(currentlyDragging));
        _widgets.add(currentlyDragging);
        currentlyDragging.dragging = true;
      }
      lastX = x;
      lastY = y;
    }
    if (eventType == 1 && currentlyDragging != null) {
      double xOffset = x - lastX;
      double yOffset = y - lastY;
      lastX = x;
      lastY = y;
      currentlyDragging.posX += xOffset;
      currentlyDragging.posY += yOffset;
      if (currentlyDragging.posX < 0) currentlyDragging.posX = 0;
      if (currentlyDragging.posX > sizeX - currentlyDragging.width - _xOffset * 2) currentlyDragging.posX = sizeX - currentlyDragging.width - _xOffset * 2;
      if (currentlyDragging.posY < 0) currentlyDragging.posY = 0;
      if (currentlyDragging.posY > sizeY - currentlyDragging.height - _yOffset * 2) currentlyDragging.posY = sizeY - currentlyDragging.height - _yOffset * 2;
    }
    if (eventType == 2) {
      print('End Drag');
      currentlyDragging.dragging = false;
      double gridOffsetX = currentlyDragging.posX % 25.0;
      double gridOffsetY = currentlyDragging.posY % 25.0;
      if (gridOffsetX > 12.5) {
        currentlyDragging.posX += 25.0 - gridOffsetX;
      } else {
        currentlyDragging.posX -= gridOffsetX;
      }
      if (gridOffsetY > 12.5) {
        currentlyDragging.posY += 25.0 - gridOffsetY;
      } else {
        currentlyDragging.posY -= gridOffsetY;
      }
      //currentlyDragging == null;
    }
    //Widget w1 = isWidgetAtPosition(x, y);
  }

  Panel isWidgetAtPosition(double x, double y) {
    for (Panel panel in _widgets.reversed) {
      double posX = panel.getWidget(_xOffset, _yOffset).left;
      double posY = panel.getWidget(_xOffset, _yOffset).top;
      double posXOffset = panel.width + posX;
      double posYOffset = panel.height + posY;
      if (x >= posX && x <= posXOffset && y >= posY && y <= posYOffset) {
        return panel;
      }
    }
    return null;
  }

  Future<List<Widget>> getWidgets() async {
    List<Widget> _toReturn = List();
    _widgets.forEach((element) {
      _toReturn.add(element.getWidget(_xOffset, _yOffset));
    });
    return _toReturn;
  }

  int getWidgetListLength() {
    return _widgets.length;
  }

  void addWidget(LayoutBloc layoutBloc) {
    print('addWidget');
    double posX = 250.0;
    double posY = 450.0;
    double height = 300.0;
    double width = 600.0;
    _widgets.add(TestPanel(null, null, null, posX, posY, height, width, layoutBloc));
  }

  double get xOffset => _xOffset;

  set xOffset(double value) {
    _xOffset = value;
  }

  double get sizeY => _sizeY;

  set sizeY(double value) {
    _sizeY = value;
  }

  double get sizeX => _sizeX;

  set sizeX(double value) {
    _sizeX = value;
  }

  double get yOffset => _yOffset;

  set yOffset(double value) {
    _yOffset = value;
  }
}