import 'package:data_vis/blocs/bloc.dart';
import 'package:data_vis/widgets/panel_type.dart';
import 'package:flutter/material.dart';

abstract class Panel {
  int id;
  PanelType type;
  Stream dataStream;
  double posX;
  double posY;
  double height;
  double width;
  bool dragging = false;
  bool showConfig = false;
  LayoutBloc layoutBloc;

  Panel(this.id, this.type, this.dataStream, this.posX, this.posY, this.height,
      this.width, this.layoutBloc);

  Positioned getWidget(double _xOffset, double _yOffset) {
    return Positioned(
      top: posY + _yOffset,
      left: posX + _xOffset,
      child: Container(
          height: height,
          width: width,
          child: AnimatedPanelPart(
            dragging: dragging,
            child: getGraphWidget(),
          )),
    );
  }

  Widget getGraphWidget();

  @override
  String toString() {
    return 'Panel{id: $id, type: $type, dataStream: $dataStream, posX: $posX, posY: $posY, height: $height, width: $width, layoutBloc: $layoutBloc}';
  }
}

class AnimatedPanelPart extends StatefulWidget {
  final Widget child;
  final bool dragging;
  AnimatedPanelPart({this.child, this.dragging});
  @override
  State<StatefulWidget> createState() {
    return AnimatedPanelPartState(child: child, dragging: dragging);
  }
}

class AnimatedPanelPartState extends State<AnimatedPanelPart> {
  Widget child;
  bool dragging;
  bool _showFirst = true;
  AnimatedPanelPartState({this.child, this.dragging});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onLongPress: () {
        _showFirst = !_showFirst;
      },
      child: Card(
        elevation: dragging ? 9.0 : 1.5,
        child: AnimatedCrossFade(
          firstChild: child,
          secondChild: configPage(),
          duration: Duration(milliseconds: 300),
          crossFadeState:
              _showFirst ? CrossFadeState.showFirst : CrossFadeState.showSecond,
        ),
      ),
    );
  }

  Widget configPage() {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          configButton(),
          deleteButton(),
          backButton(),
        ],
      ),
    );
  }

  Widget backButton() {
    return FlatButton(
      child: Row(
        children: <Widget>[
          Icon(Icons.arrow_back),
          Text('Go back'),
        ],
        mainAxisAlignment: MainAxisAlignment.center,
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30.0)
      ),
      onPressed: () {
        print('backPressed');
      },
    );
  }

  Widget configButton() {
    return FlatButton(
      child: Row(
        children: <Widget>[Icon(Icons.settings), Text('Config panel')],
        mainAxisAlignment: MainAxisAlignment.center,
      ),
      onPressed: () {
        print('Config pressed');
      },
      shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
    );
  }

  Widget deleteButton() {
    return FlatButton(
      child: Row(
        children: <Widget>[Icon(Icons.delete_forever), Text('Delete panel')],
        mainAxisAlignment: MainAxisAlignment.center,
      ),
      onPressed: () {
        print('Delete pressed');
      },
      color: Colors.red,
      hoverColor: Colors.red,
      shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
    );
  }
}
