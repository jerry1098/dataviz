import 'package:data_vis/backend/DataHandler.dart';
import 'package:data_vis/pages/main_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:data_vis/blocs/bloc.dart';

class ConfigForm extends StatefulWidget {

  final DataHandler dataHandler;

  ConfigForm({Key key, this.dataHandler}) : super(key: key);

  @override
  _ConfigFormState createState() => _ConfigFormState(dataHandler: dataHandler);
}

class _ConfigFormState extends State<ConfigForm> {

  DataHandler dataHandler;

  _ConfigFormState({@required this.dataHandler});

  final _portController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    _configButtonPressed(String string) {
      //print(_portController.text);
      BlocProvider.of<ConfigBloc>(context).add(
          ConfigButtonPressed(
              port: _portController.text));
    }

    return BlocListener<ConfigBloc, ConfigState>(
      listener: (context, state) {
        if (state is ErrorConfigState) {
          Scaffold.of(context).showSnackBar(SnackBar(
            content: Text('${state.error}'),
            backgroundColor: Colors.red,
          ));
        } else if (state is InitializedConfigState) {
          Scaffold.of(context).showSnackBar(SnackBar(
            content: Text('Initialized', style: TextStyle(color: Colors.black),),
            backgroundColor: Colors.white,
          ));
          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => MainPage(dataHandler: dataHandler)));
        } else if (state is WrongPortConfigState) {
          Scaffold.of(context).showSnackBar(SnackBar(
            content: Text('Wrong port input... \ninput must be a vaild port between 1 and 65,535', style: TextStyle(color: Colors.black),),
            backgroundColor: Colors.orangeAccent,
          ));
        } else if (state is NoPortConfigState) {
          Scaffold.of(context).showSnackBar(SnackBar(
            content: Text('No port given... \ninitializing with default port', style: TextStyle(color: Colors.black),),
            backgroundColor: Colors.white,
          ));
        }
      },
      child: BlocBuilder<ConfigBloc, ConfigState>(
        builder: (context, state) {
          return Form(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                TextFormField(
                  autofocus: true,
                  onFieldSubmitted: (content) {
                    BlocProvider.of<ConfigBloc>(context).add(
                        ConfigButtonPressed(
                            port: _portController.text));
                  },
                  decoration: InputDecoration(
                      labelText: 'Port Number (leave empty for default 4444)'),
                  //keyboardType: TextInputType.numberWithOptions(decimal: false, signed: false),
                  controller: _portController,
                ),
                state is! InitializingConfigState && state is! NoPortConfigState
                    ? RaisedButton(
                        onPressed: () {
                          BlocProvider.of<ConfigBloc>(context).add(
                              ConfigButtonPressed(
                                  port: _portController.text));
                        },
                        child: Text('Start', style: TextStyle(color: Color.fromRGBO(219, 237, 243, 1))),
                        color: Color.fromRGBO(0, 129, 138, 1),
                        padding: EdgeInsets.fromLTRB(60.0, 25.0, 60.0, 25.0),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0)),
                      )
                    : Container(
                        child: state is InitializingConfigState || state is NoPortConfigState
                            ? CircularProgressIndicator()
                            : null,
                      ),
              ],
            ),
          );
        },
      ),
    );
  }
}
