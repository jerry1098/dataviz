import 'package:data_vis/backend/DataHandler.dart';
import 'package:data_vis/widgets/config_form.dart';
import 'package:flutter/material.dart';

class ConfigCard extends StatefulWidget {

  final DataHandler dataHandler;

  ConfigCard({@required this.dataHandler});

  @override
  State<StatefulWidget> createState() {
    return _ConfigCardState(dataHandler: dataHandler);
  }
}

class _ConfigCardState extends State<ConfigCard> {

  DataHandler dataHandler;

  _ConfigCardState({@required this.dataHandler});

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0)
      ),
      color: Color.fromRGBO(64, 75, 105, 1),
      child: Container(
        margin: EdgeInsets.all(30.0),
        child: ConfigForm(dataHandler: dataHandler),
      ),
    );
  }
}
