import 'dart:async';

import 'package:data_vis/blocs/bloc.dart';
import 'package:data_vis/painter/DrawGrid.dart';
import 'package:flutter/material.dart';
import 'dart:math';

import 'package:flutter_bloc/flutter_bloc.dart';

class ParentGrid extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ParentGridState();
  }
}

class ParentGridState extends State<ParentGrid> {
  @override
  Widget build(BuildContext context) {
    _Grid _grid = _Grid();
    _grid.startTimer();
    return GestureDetector(
      onHorizontalDragStart: (update) {
        //print('Start drag at ${update.localPosition.dx} and ${update.localPosition.dy}');
        BlocProvider.of<LayoutBloc>(context).add(GestureLayoutEvent(
            x: update.localPosition.dx,
            y: update.localPosition.dy,
            eventType: 0));
      },
      onHorizontalDragUpdate: (update) {
        //BlocProvider.of<LayoutBloc>(context).add(GestureLayoutEvent(offsetX: update.localPosition.dx, offsetY: update.localPosition.dy));
        //print('Update drag at ${update.localPosition.dx} and ${update.localPosition.dy}');
        BlocProvider.of<LayoutBloc>(context).add(GestureLayoutEvent(
            x: update.localPosition.dx,
            y: update.localPosition.dy,
            eventType: 1));
      },
      onHorizontalDragEnd: (update) {
        BlocProvider.of<LayoutBloc>(context)
            .add(GestureLayoutEvent(x: 0.0, y: 0.0, eventType: 2));
      },
      onHorizontalDragCancel: () {
        BlocProvider.of<LayoutBloc>(context)
            .add(GestureLayoutEvent(x: 0.0, y: 0.0, eventType: 2));
      },
      child: BlocListener<LayoutBloc, LayoutState>(
        listener: (context, state) {},
        child: BlocBuilder<LayoutBloc, LayoutState>(
          builder: (context, state) {
            if (state is InitializedLayoutState) {
              if (state.gridVisible) _grid.showGrid();
              List<Widget> children = List();
              children.add(_grid);
              children.addAll(state.widgets);
              return Container(
                constraints: BoxConstraints.expand(),
                child: Stack(
                  children: children,
                ),
              );
            }
            if (state is InitialLayoutState) {
              return Container(
                  constraints: BoxConstraints.expand(),
                  child: Stack(
                    children: <Widget>[
                      _grid,
                      Center(
                        child: Text(
                          'Please add a Panel with the + button in the upper left corner',
                          style: TextStyle(
                            color: Colors.white70,
                            fontSize: 40.0,
                          ),
                        ),
                      )
                    ],
                  ));
            }
            return Center(child: CircularProgressIndicator());
          },
        ),
      ),
    );
  }
}

class _Grid extends StatefulWidget {
  final _GridState state = _GridState();
  void showGrid() {
    return state.showGrid();
  }

  void startTimer() {
    return state.startTimer();
  }

  @override
  State<StatefulWidget> createState() {
    return state;
  }
}

class _GridState extends State<_Grid> with SingleTickerProviderStateMixin {
  double _gridOpacity = 0;
  AnimationController controller;
  Animation<double> _animation;
  double timeToShowGrid = 0.0;

  void showGrid() {
    timeToShowGrid = 1.0;
  }

  void startTimer() {
    Timer.periodic(Duration(milliseconds: 200), (timer) {
      if (timeToShowGrid > 0.0) {
        controller.forward();
        timeToShowGrid -= 0.2;
      } else if (timeToShowGrid <= 0.0) {
        controller.reverse();
        timeToShowGrid = 0.0;
      }
    });
  }

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: Duration(milliseconds: 100), vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    _animation = Tween(begin: 0.0, end: 0.1).animate(controller)
      ..addListener(() {
        setState(() {
          _gridOpacity = _animation.value;
        });
      });
    return Container(
      constraints: BoxConstraints.expand(),
      child: CustomPaint(
        painter: DrawGrid(
            layoutBloc: BlocProvider.of<LayoutBloc>(context),
            opacity: _gridOpacity),
      ),
    );
  }
}
