enum PanelType {
  BarChart,
  TimeSeriesChart,
  LineChart,
  ScatterPlotChart,
  ComboChart,
  PieChart,
}