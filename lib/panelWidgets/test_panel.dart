import 'package:data_vis/blocs/layout_bloc.dart';
import 'package:data_vis/widgets/panel.dart';
import 'package:data_vis/widgets/panel_type.dart';
import 'package:flutter/material.dart';

class TestPanel extends Panel {

  TestPanel(int id, PanelType type, Stream dataStream, double posX, double posY, double height, double width, LayoutBloc layoutBloc) : super(id, type, dataStream, posX, posY, height, width, layoutBloc);

  @override
  Widget getGraphWidget() {
    return Text('ToDo');
  }
}
